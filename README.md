# Ce sera en "petite salle de réunion" au CEFE, 1er étage aile B.

# Pour celles et ceux qui seront en visio, ce sera ici: https://univ-lyon1-fr.zoom.us/j/86272922712

# MOTIVATION

**Exemple 1 :**
```
library(umap)
...
...
Error: Detected umap-learn 0.5.2 but last supported version is 0.4
```
(résolu depuis peu)

**Exemple 2 :**

https://www.tensorflow.org/api_docs/python/tf/keras/applications/efficientnet_v2/EfficientNetV2B3

```
TensorFlow > API > TensorFlow Core v2.8.0 > Python

tf.keras.applications.efficientnet_v2.EfficientNetV2B3
```

https://rdrr.io/github/rstudio/keras/man/application_efficientnet.html

```
application_efficientnet_b3(
  include_top = TRUE,
  weights = "imagenet",
  input_tensor = NULL,
  input_shape = NULL,
  pooling = NULL,
  classes = 1000L,
  classifier_activation = "softmax",
  ...
)
```

**Exemple 3 :**
```
> library(packagefinder)
> findPackage("cleanlab")
Your are searching packages for the term 'cleanlab'

Please wait while index is being searched...

No results found.
```

# INSTALLER PYTHON et SES MODULES

### Solution 1 - Utiliser `pip`

<img src="img/python1.jpg" alt="isolated" width="400"/>
<img src="img/python3.jpg" alt="isolated" width="400"/>
<img src="img/pip1.jpg" alt="isolated" width="400"/>
<img src="img/pip2.jpg" alt="isolated" width="600"/>

### Solution 2 - Utiliser `conda`


https://rstudio.github.io/reticulate/articles/python_packages.html

<img src="img/conda.jpg" alt="isolated" width="600"/>


# LANCER RETICULATE


```R
library(reticulate)
```


```R
Sys.which("python")
```


<strong>python:</strong> '/usr/bin/python'



```R
py_config()
```


    python:         /usr/bin/python3
    libpython:      /usr/lib/libpython3.10.so
    pythonhome:     //usr://usr
    version:        3.10.1 (main, Dec 18 2021, 23:53:45) [GCC 11.1.0]
    numpy:          /usr/lib/python3.10/site-packages/numpy
    numpy_version:  1.21.5
    
    python versions found: 
     /usr/bin/python3
     /usr/bin/python


# EXECUTER DES LIGNES DE `PYTHON` depuis `R`


```R
py_run_string("x = 4")
py$x
```


4



```R
py_run_file("scripts/dummy.py")
py$y
```


5



```R
source_python("scripts/add.py")
pyadd(4,5)
pyadd(py$x,py$y)
```


9



9


# CHARGER DES MODULES PYTHON (avec la bonne syntaxe...)

Attention, remplacer l'utilisation du `.` en `Python` par celle du `$` en `R/reticulate` 


```R
math <- import("math")
math$log(2.7182)
```


0.99996989653911



```R
np <- import("numpy")
np$argsort(c(6.3,1.5,4.3))
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>1</li><li>2</li><li>0</li></ol>




```R
np$random$normal()
```


1.4908981890128


# CONVERSION DE TYPE entre `Python` et `R`
https://rstudio.github.io/reticulate/articles/calling_python.html


| R | Python | Examples |
| --- | --- | --- |
| Single-element vector | Scalar | `1, 1L, TRUE, "foo"`
| Multi-element vector | List | `c(1.0, 2.0, 3.0), c(1L, 2L, 3L)`
| List of multiple types | Tuple | `list(1L, TRUE, "foo")`
| Named list | Dict | `list(a = 1L, b = 2.0), dict(x = x_data)`
| Matrix/Array| NumPy ndarray | `matrix(c(1,2,3,4), nrow = 2, ncol = 2)`
| Data Frame | Pandas DataFrame | `data.frame(x = c(1,2,3), y = c("a", "b", "c"))`
| NULL, TRUE, FALSE | None, True, False | `NULL, TRUE, FALSE`

### Depuis `Python` vers `R`
Par defaut, quand des objets `Python` sont retournés vers R, ils sont convertis dans leur type equivalent `R`.


```R
py_run_string("import numpy; ar = numpy.zeros(5)")
py$ar
class(py$ar)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>0</li><li>0</li><li>0</li><li>0</li><li>0</li></ol>




'array'



```R
# import numpy and specify automatic Python to R conversion
np <- import("numpy")
ar <- np$array(c(1:4))
class(ar)
```


'array'



```R
## import numpy and specify no automatic Python to R conversion, to keep playing in Python
np <- import("numpy", convert = FALSE)
npar <- np$array(c(1:4))
class(npar)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'numpy.ndarray'</li><li>'python.builtin.object'</li></ol>




```R
ar <- py_to_r(npar)
class(ar)
```


'array'



```R
## ATTENTION
print(npar[0])
print(npar[1])
print(ar[0])
```

    1
    2
    numeric(0)


### Depuis `R` vers `Python`
La plupart du temps, la conversion est automatique. Mais attention...



```R
## ATTENTION
source_python("scripts/range.py")
pyrange(5)
```


    Error in py_call_impl(callable, dots$args, dots$keywords): TypeError: 'float' object cannot be interpreted as an integer
    
    Detailed traceback:
      File "<string>", line 2, in pyrange
    
    Traceback:


    1. pyrange(5)

    2. py_call_impl(callable, dots$args, dots$keywords)



```R
pyrange(5L)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>0</li><li>1</li><li>2</li><li>3</li><li>4</li></ol>




```R
filename <- "img/olivier.jpg"
py <- py_run_string(paste0("
from tensorflow.keras.applications.efficientnet import preprocess_input
from tensorflow.keras.preprocessing import image
import numpy as np
img = image.load_img(\"", filename ,"\", target_size=(300,300,3))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)")
)
dim(py$x)
class(py$x)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>1</li><li>300</li><li>300</li><li>3</li></ol>




'array'



```R
image(t(py$x[1,300:1,1:300,1]))
```


    
![png](atelier040422_files/atelier040422_29_0.png)
    


# UMAP

<img src="img/umap.jpg" alt="isolated" width="400"/>

<img src="img/umapbotella.jpg" width="500"/> (source: Botella et al, MEE 2021)

Cf. `How UMAP Works` (https://umap-learn.readthedocs.io/en/latest/how_umap_works.html)

#### Exemple 1 (Version Python)
```
import numpy as np
import matplotlib.pyplot as plt
import umap

np.random.seed(42)
data = np.random.rand(800, 4)

fit = umap.UMAP()
u = fit.fit_transform(data)

plt.scatter(u[:,0], u[:,1], c=data)
plt.title('UMAP embedding of random colours');
```

#### Exemple 2 (Version R)


```R
library(reticulate)
np <- import("numpy")
umap <- import("umap")
```


```R
np$random$seed(42)
```


    Error in py_call_impl(callable, dots$args, dots$keywords): TypeError: Cannot cast scalar from dtype('float64') to dtype('int64') according to the rule 'safe'
    
    Detailed traceback:
      File "mtrand.pyx", line 246, in numpy.random.mtrand.RandomState.seed
      File "_mt19937.pyx", line 166, in numpy.random._mt19937.MT19937._legacy_seeding
      File "_mt19937.pyx", line 186, in numpy.random._mt19937.MT19937._legacy_seeding
    
    Traceback:


    1. np$random$seed(42)

    2. py_call_impl(callable, dots$args, dots$keywords)



```R
np$random$seed(42L) 
```


```R
data <- np$random$rand(800L, 4L)
np$shape(data)
```


<ol>
	<li>800</li>
	<li>4</li>
</ol>




```R
fit <-umap$UMAP()
u <- fit$fit_transform(data)
```


```R
plt <- import("matplotlib.pyplot")
plt$scatter(u[,1], u[,2], c=data)
plt$title('UMAP embedding of random colours');
plt$show()
```


    <matplotlib.collections.PathCollection>



    Text(0.5, 1.0, 'UMAP embedding of random colours')


#### Exemple 2


```R
library(reticulate)
np <- import("numpy")
umap <- import("umap")
```


```R
load("data/releves.rda")
str(releves)
heatmap(releves)
```

     num [1:300, 1:100] 0 50 100 80 80 20 0 0 80 0 ...
     - attr(*, "dimnames")=List of 2
      ..$ : chr [1:300] "num1" "num2" "num3" "num4" ...
      ..$ : chr [1:100] "SP1" "SP2" "SP3" "SP4" ...



    
![png](atelier040422_files/atelier040422_41_1.png)
    



```R
fit <- umap$UMAP(n_neighbors=25, n_components=2, metric='braycurtis')
```


```R
fit <- umap$UMAP(n_neighbors=25L, n_components=2L, metric='braycurtis')
```


```R
u <- fit$fit_transform(np$array(releves))
str(u)
```

     num [1:300, 1:2] 14,1 12,6 13,3 13,8 13,5 ...



```R
plot(u[,1], u[,2])
```


    
![png](atelier040422_files/atelier040422_45_0.png)
    

