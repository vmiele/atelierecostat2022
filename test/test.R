library(reticulate)
Sys.which("python")
use_python("/usr/local/bin/python")
py_config()

py_run_string("x = 4")
py$x

py_run_string("v = [i for i in range(5)]")
py$v

py_run_string("import numpy; ar = numpy.zeros(5)")
py$ar
class(py$ar)

py_run_file("scripts/dummy.py")
py$y

source_python("scripts/add.py")
pyadd(4,5)
pyadd(py$x,py$y)


##########################################
## APPEL module Python avec syntaxe "$"
math <- import("math")
math$log(2.7182)


np <- import("numpy")
np$argsort(c(6.3,1.5,4.3))
np$random$normal()



##########################################
## CONVERSION de TYPE DEPUIS PYTHON VERS R
# By default when Python objects are returned to R they are converted to their equivalent R types
# import numpy and specify automatic Python to R conversion
np <- import("numpy")
ar <- np$array(c(1:4))
class(ar)

## import numpy and specify no automatic Python to R conversion, to keep playing in Python
np <- import("numpy", convert = FALSE)
npar <- np$array(c(1:4))
class(npar)
ar <- py_to_r(npar)
class(ar)

## ATTENTION
npar[0]
npar[1]
ar[0]

##########################################
## CONVERSION de TYPE DEPUIS R vers PYTHON
source_python("scripts/range.py")
pyrange(5)
pyrange(5L)


##########################################
filename <- "img/olivier.jpg"
py <- py_run_string(paste0("
from tensorflow.keras.applications.efficientnet import preprocess_input
from tensorflow.keras.preprocessing import image
import numpy as np
img = image.load_img(\"", filename ,"\", target_size=(300,300,3))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)")
)
dim(py$x)
image(t(py$x[1,300:1,1:300,1]))


##########################################
## https://umap-learn.readthedocs.io/en/latest/parameters.html
np <- import("numpy")
umap <- import("umap")
np$random$seed(42)
np$random$seed(42L) 
data <- np$random$rand(800L, 4L)
np$shape(data)
fit <-umap$UMAP()
u <- fit$fit_transform(data)
plt <- import("matplotlib.pyplot")
plt$scatter(u[,1], u[,2], c=data)
plt$title('UMAP embedding of random colours');
plt$show()

library(reticulate)
np <- import("numpy")
umap <- import("umap")
load("data/releves.rda")
str(releves)
fit <- umap$UMAP(n_neighbors=25, n_components=2, metric='braycurtis')
u <- fit$fit_transform(np$array(releves))
fit <- umap$UMAP(n_neighbors=25L, n_components=2L, metric='braycurtis')
u <- fit$fit_transform(np$array(releves))
plot(u[,1], u[,2])
